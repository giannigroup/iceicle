/**
 * @brief miniapp to solve conservation laws
 *
 * @author Gianni Absillis (gabsill@ncsu.edu)
 */

#include "iceicle/disc/conservation_law.hpp"
#include "iceicle/anomaly_log.hpp"
#include "iceicle/dat_writer.hpp"
#include "iceicle/disc/bc_lua_interface.hpp"
#include "iceicle/disc/burgers.hpp"
#include "iceicle/disc/conservation_law_lua_interface.hpp"
#include "iceicle/disc/navier_stokes.hpp"
#include "iceicle/fespace/fespace_lua_interface.hpp"
#include "iceicle/geometry/face.hpp"
#include "iceicle/iceicle_mpi_utils.hpp"
#include "iceicle/initialization.hpp"
#include "iceicle/mesh/mesh_lua_interface.hpp"
#include "iceicle/program_args.hpp"
#include "iceicle/pvd_writer.hpp"
#include "iceicle/solvers_lua_interface.hpp"
#include "iceicle/string_utils.hpp"
#include "iceicle/mesh/mesh_partition.hpp"
// #include "iceicle/disc/ns_lua_interface.hpp"
#include <sol/table.hpp>
#ifdef ICEICLE_USE_PETSC
#elifdef ICEICLE_USE_MPI
#include "mpi.h"
#endif
#include <fenv.h>
#include <sol/sol.hpp>

// using declarations
using namespace NUMTOOL::TENSOR::FIXED_SIZE;
using namespace iceicle;
using namespace iceicle::util;
using namespace iceicle::util::program_args;
using namespace iceicle::tmp;

// Get the floating point and index types from
// cmake configuration
using T = build_config::T;
using IDX = build_config::IDX;

/// @brief class for testing dirichlet implementation
/// TODO: move to a unit test
template <class T, int ndim, int neq> class test_dirichlet {
public:
  using value_type = T;
  static constexpr int nv_comp = neq;
  static constexpr int dimensionality = ndim;

  std::vector<std::function<void(const T *, T *)>> dirichlet_callbacks;

  auto operator()() -> void {
    std::array<T, ndim> x{};
    std::cout << "x: [ ";
    for (int idim = 0; idim < ndim; ++idim) {
      x[idim] = (0.125) * (idim + 1);
      std::cout << x[idim] << " ";
    }
    std::cout << "]" << std::endl;

    for (auto f : dirichlet_callbacks) {
      std::cout << "f: [ ";
      std::array<T, neq> fval{};
      f(x.data(), fval.data());
      for (int ieq = 0; ieq < neq; ++ieq) {
        std::cout << fval[ieq] << " ";
      }
      std::cout << " ]" << std::endl;
    }
    std::cout << std::endl;
  }
};

template <class T, class IDX, int ndim, int conformity, class pflux, class cflux, class dflux>
void initialize_and_solve(
    sol::table config_tbl, FESpace<T, IDX, ndim, conformity> &fespace,
    ConservationLawDDG<T, ndim, pflux, cflux, dflux> &conservation_law) {
  using DiscType = ConservationLawDDG<T, ndim, pflux, cflux, dflux>;

  // ==============================
  // = Set Discretization Options =
  // ==============================
    sol::table cons_law_tbl = config_tbl["conservation_law"];
    // discretization options
    conservation_law.sigma_ic = cons_law_tbl.get_or("sigma_ic", conservation_law.sigma_ic);
    conservation_law.interior_penalty = cons_law_tbl.get_or("interior_penalty", conservation_law.interior_penalty);
    sol::optional<T> beta0_user = cons_law_tbl["beta0"];
    if(beta0_user.has_value())
      conservation_law.beta0_user = beta0_user.value();

  // ==================================
  // = Initialize the solution vector =
  // ==================================
  constexpr int neq =
      std::remove_reference_t<decltype(conservation_law)>::nv_comp;
  fe_layout_right u_layout{fespace, to_size<neq>{}, std::true_type{}};
  std::vector<T> u_data(u_layout.size());
  fespan u{u_data.data(), u_layout};
  initialize_solution_lua(config_tbl, fespace, u);

  // ===============================
  // = Output the Initial Solution =
  // ===============================
  if constexpr(ndim == 1){
    io::DatWriter<T, IDX, ndim, conformity> dat_writer{fespace};
    dat_writer.register_fields(u, conservation_law.field_names);
    dat_writer.collection_name = "initial_condition";
    dat_writer.write_dat(0, 0.0);
  }
  if constexpr (ndim == 2 || ndim == 3) {
#ifdef ICEICLE_USE_VTK
    io::PVTUWriter<T, IDX, ndim, conformity> vtk_writer{fespace, mpi::comm_world};
    io::output_field_function<T, DiscType::nv_comp>
        field_func{conservation_law.output_field_names(), conservation_law.output_field_func()};
    vtk_writer.register_fields(u, field_func);
    vtk_writer.rename_collection("initial_condition");
    vtk_writer.write(0, 0.0);
#endif
  }

  // ==================================
  // = Set up the Boundary Conditions =
  // ==================================
  sol::optional<sol::table> bc_desc_opt = config_tbl["boundary_conditions"];
  if (bc_desc_opt) {
    sol::table bc_tbl = bc_desc_opt.value();
    add_dirichlet_callbacks(conservation_law, bc_tbl);
    add_neumann_callbacks(conservation_law, bc_tbl);
  }

  // ========================
  // = Add User Source Term =
  // ========================
  solvers::add_source_term_callback(conservation_law, config_tbl);

  // =========
  // = Solve =
  // =========
  sol::optional<sol::table> mdg_tbl = config_tbl["mdg"];
  IDX ncycles = (mdg_tbl) ? mdg_tbl.value().get_or("ncycles", 1) : 1;
  for (IDX icycle = 0; icycle < ncycles; ++icycle) {

    mpi::execute_on_rank(0, [&] {
      std::cout << "==============" << std::endl;
      std::cout << "Cycle: " << icycle << std::endl;
      std::cout << "==============" << std::endl;
    });
    auto geo_map = solvers::lua_select_mdg_geometry(
        config_tbl, fespace, conservation_law, icycle, u);
    solvers::lua_solve(config_tbl, fespace, geo_map, conservation_law, u);
  }

  // ============================
  // = Post-Processing/Analysis =
  // ============================
  solvers::lua_error_analysis(config_tbl, fespace, conservation_law, u);
}

template <int ndim>
[[gnu::noinline]]
void setup(sol::table script_config, cli_parser cli_args) {
  // ==============
  // = Setup Mesh =
  // ==============
  auto mesh_opt = construct_mesh_from_config<T, IDX, ndim>(script_config);
  if (!mesh_opt){
    std::cerr << "Mesh construction failed..." << std::endl;
    AnomalyLog::handle_anomalies();
    return; // exit if we have no valid mesh
  }
  AbstractMesh<T, IDX, ndim> mesh = mesh_opt.value();
  std::vector<IDX> invalid_faces;
  if (!validate_normals(mesh, invalid_faces)) {
    std::cout << "invalid normals on the following faces: ";
    for (IDX ifac : invalid_faces)
      std::cout << ifac << ", ";
    std::cout << "\n";
    return;
  }
  perturb_mesh(script_config, mesh);
  manual_mesh_management(script_config, mesh);

  AbstractMesh<T, IDX, ndim> pmesh{partition_mesh(mesh)};

  if(AnomalyLog::size() > 0){
    std::cerr << "Errors from setting up mesh: ";
    AnomalyLog::handle_anomalies(std::cerr);
    return;
  }

  if (cli_args["debug1"]) {
    // linear advection a = [0.2, 0];
    // 2 element mesh on [0, 1]^2
    pmesh.coord[7][0] = 0.7;
    pmesh.coord[4][0] = 0.55;
  }
  if (cli_args["debug2"]) {
    pmesh.coord[4][0] = 0.69;
  }

  // ===================================
  // = create the finite element space =
  // ===================================
  sol::table fespace_tbl = script_config["fespace"];
  auto fespace = lua_fespace(&pmesh, fespace_tbl);
  fespace.print_info(std::cout);

  // ============================
  // = Setup the Discretization =
  // ============================
  if (script_config["conservation_law"].valid()) {
    sol::table cons_law_tbl = script_config["conservation_law"];

    if (eq_icase(cons_law_tbl["name"].get<std::string>(), "burgers")) {
      ConservationLawDDG disc{lua::set_up_burgers<T, ndim>(cons_law_tbl)};
      initialize_and_solve(script_config, fespace, disc);
    } else if (eq_icase(cons_law_tbl["name"].get<std::string>(),
                        "spacetime-burgers")) {
      ConservationLawDDG disc{lua::set_up_st_burgers<T, ndim>(cons_law_tbl)};
      initialize_and_solve(script_config, fespace, disc);

    } else if (eq_icase(cons_law_tbl["name"].get<std::string>(),
                            "navier-stokes")) {

      // set up the physics
      auto physics_opt = navier_stokes::lua::get_physics<T, ndim>(cons_law_tbl);
      if(!physics_opt){
        AnomalyLog::log_anomaly("Could not set up physics for NS");
        return;
      }
      std::visit(
        tmp::select_fcn{
            [&](auto&& phys) -> void { 
                navier_stokes::Physics physics{phys};

                // get isothermal wall temperatures
                sol::optional<sol::table> iso_tmps_opt = cons_law_tbl["isothermal_temperatures"];
                if(iso_tmps_opt){
                    sol::table iso_tmps = iso_tmps_opt.value();
                    for(int i = 0; i < iso_tmps.size(); ++i){
                        physics.isothermal_temperatures.push_back(iso_tmps[i + 1]);
                    }
                }

                // Create the physical and diffusion fluxes
                navier_stokes::Flux flux{physics, std::true_type{}};
                navier_stokes::DiffusionFlux diffusion_flux{physics, std::true_type{}};

                // select the inviscid flux function 
                std::string flux_name = cons_law_tbl.get_or("flux", std::string{"van_leer"});

                if(util::eq_icase(flux_name, "van_leer")){
                    navier_stokes::VanLeer numflux{physics};
                    ConservationLawDDG disc{
                        std::move(flux), std::move(numflux), std::move(diffusion_flux)};
                    initialize_and_solve(script_config, fespace, disc);
                    return;
                }

                util::AnomalyLog::log_anomaly("Could not construct NS conservation law");
                return;
            }
        }, 
        physics_opt.value()
    );
//      } else {
//        // check to make sure no isothermal boundary conditions are present
//        for(auto trace : fespace.get_boundary_traces()){
//          if(trace.face->bctype == BOUNDARY_CONDITIONS::NO_SLIP_ISOTHERMAL){
//            std::cerr << "Isothermal boundary condition on face " << trace.facidx
//              << " but no entries in isothermal_temperatures" << std::endl;
//            return;
//          }
//        }
//      }
//
    } else if (eq_icase(cons_law_tbl["name"].get<std::string>(),
                            "euler")) {
      // set up the physics
      auto physics_opt = navier_stokes::lua::get_physics<T, ndim>(cons_law_tbl);
      if(!physics_opt){
        AnomalyLog::log_anomaly("Could not set up physics for NS");
        return;
      }
      physics_opt.value() >> tmp::select_fcn{
          [&](auto&& phys) -> void { 
              navier_stokes::Physics physics{phys};

              // get isothermal wall temperatures
              sol::optional<sol::table> iso_tmps_opt = cons_law_tbl["isothermal_temperatures"];
              if(iso_tmps_opt){
                  sol::table iso_tmps = iso_tmps_opt.value();
                  for(int i = 0; i < iso_tmps.size(); ++i){
                      physics.isothermal_temperatures.push_back(iso_tmps[i + 1]);
                  }
              }

              // Create the physical and diffusion fluxes
              navier_stokes::Flux flux{physics, std::false_type{}};
              navier_stokes::DiffusionFlux diffusion_flux{physics, std::false_type{}};

              // select the inviscid flux function 
              std::string flux_name = cons_law_tbl.get_or("flux", std::string{"van_leer"});

              if(util::eq_icase(flux_name, "van_leer")){
                  navier_stokes::VanLeer numflux{physics};
                  ConservationLawDDG disc{
                      std::move(flux), std::move(numflux), std::move(diffusion_flux)};
                      initialize_and_solve(script_config, fespace, disc);
                      return;
              }

              util::AnomalyLog::log_anomaly("Could not construct Euler conservation law");
              return;
          }
      };
    } else {
      AnomalyLog::log_anomaly(
          Anomaly{"No such conservation_law implemented",
                  text_not_found_tag{cons_law_tbl["name"].get<std::string>()}});
    }

  } else {
    std::cout << "No conservation_law table specified: exiting...";
  }
}

int main(int argc, char *argv[]) {

  mpi::init(&argc, &argv);

  // ===============================
  // = Command line argument setup =
  // ===============================
  cli_parser cli_args{argc, argv};

  cli_args.add_options(
      cli_flag{"help", "print the help text and quit."},
      cli_flag{"enable_fp_except",
               "enable floating point exceptions (ignoring FE_INEXACT)"},
      cli_option{"scriptfile", "The file name for the lua script to run",
                 parse_type<std::string_view>{}},
      cli_flag{"debug1", "internal debug flag"},
      cli_flag{"debug2", "internal debug flag"});
  if (cli_args["help"]) {
    cli_args.print_options(std::cout);
    return 0;
  }
  if (cli_args["enable_fp_except"]) {
    feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);
  }

  // =============
  // = Lua Setup =
  // =============

  // The default file to read as input deck
  const char *default_input_deck_filename = "iceicle.lua";

  // Parse the input deck
  sol::state lua_state;
  lua_state.open_libraries(sol::lib::base);
  lua_state.open_libraries(sol::lib::package);
  lua_state.open_libraries(sol::lib::math);

  sol::optional<sol::table> script_config_opt;
  if (cli_args["scriptfile"]) {
    script_config_opt =
        lua_state.script_file(cli_args["scriptfile"].as<std::string>());
  } else {
    script_config_opt = lua_state.script_file(default_input_deck_filename);
  }

  sol::table script_config;
  if (script_config_opt) {
    script_config = script_config_opt.value();
  } else {
    std::cerr << "Error loading script configuration" << std::endl;
    return 1;
  }

  //    // template specialization: ndim
  int ndim_arg = script_config["ndim"];
  if(mpi::mpi_world_rank() == 0)
    std::cout << "ndim: " << ndim_arg << std::endl;
  switch (ndim_arg) {
  case 1:
    setup<1>(script_config, cli_args);
    break;
  case 2:
    setup<2>(script_config, cli_args);
    break;
  }
  //    // invoke the ndim function
  //    WHY DOES THIS BREAK STUFF >:3
  //    NUMTOOL::TMP::invoke_at_index(
  //        NUMTOOL::TMP::make_range_sequence<int, 1, 3>{},
  //        ndim_arg,
  //        ndim_func);

  mpi::finalize();
  AnomalyLog::handle_anomalies();
  return 0;
}
