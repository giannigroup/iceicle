.. ICEicle documentation master file, created by
   sphinx-quickstart on Thu Apr 25 12:04:09 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role:: cpp(code)
   :language: cpp

Welcome to ICEicle's documentation!
===================================

Introduction
============
ICEicle is a research finite element code aimed at implementing MDG-ICE.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   technical_background
   iceicle_api
   lua_api






